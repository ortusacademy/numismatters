# Express-TypeORM-React-boilerplate

This is a boilerplate project using the following technologies:

- [React](https://facebook.github.io/react/) and [React Router](https://reacttraining.com/react-router/) for the frontend
- [Express](http://expressjs.com/) and [TypeORM](https://github.com/sequelize/sequelize)\* for the backend
- [Sass](http://sass-lang.com/) for styles (using the SCSS syntax)
- [pm2](https://pm2.io) for running and deploying the server

\* For examples on good TypeORM practices read [TypeORM By Example](https://codeburst.io/typeorm-by-example-part-1-6d6da04f9f23) tutorial.

## Requirements

- [Node.js](https://nodejs.org/en/) 8+
- [yarn](https://yarnpkg.com)
- [pm2](https://pm2.io)

```shell
npm install -g yarn
npm install -g pm2 (required - server)
yarn
```

---

A [dotenv](https://www.npmjs.com/package/dotenv) file is used to configure application secrets and build mode (development | production).

You must **copy** `.env.example`, and **rename** it `.env`.

## Running

To compile the code for production, ensure NODE_ENV is set to `"production"` in `.env` file. In production Express serves the front-end as well as the API under the same port.

Run in production mode:

```shell
yarn build
yarn deploy
```

---

To compile the code for development, ensure NODE_ENV is set to `"development"` in `.env` file.

Development mode:

```shell
yarn dev
```

In development mode the Express server only serves API routes, and is automatically restarted if any files changes under `src/server` directory change. The React front-end is served from a [ParcelProxyServer](https://www.npmjs.com/package/parcel-proxy-server) with HMR (hot module replacement) enabled.

The front-end runs on http://localhost:8080, and proxies requests to `http://localhost:{process.env.PORT}`

The back-end runs on `http://localhost:{process.env.PORT}`

## Digital Ocean Deployment

sudo systemctl restart nginx 
sudo nano /etc/nginx/sites-available/default
yarn deploy

~/NumisMatters

rm /mnt/volume_nyc1_03/db.sqlite
