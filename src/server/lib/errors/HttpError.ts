/**
 * @file Contains HttpError class that can be used to capture server errors.
 * @author Samuel Batista
 * {@link https://kostasbariotis.com/rest-api-error-handling-with-express-js/}
 */
import { StatusCode } from '../../../shared/StatusCode';

export class HttpError extends Error {
	name = 'HttpError';
	status = StatusCode.Error;
	chalk = require('chalk');
	constructor(message: string) {
		super(message || 'There was an unspecified server error');
		this.stack = this.chalk.bold.red(this.stack);
	}
}
