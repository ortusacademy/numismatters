
/**
 * @file Contains NotFound class that can be used to capture resource errors.
 * @author Samuel Batista
 * {@link https://kostasbariotis.com/rest-api-error-handling-with-express-js/}
 */
import { StatusCode } from '../../../shared/StatusCode';

export class NotFound extends Error {
	name = 'NotFound';
	status = StatusCode.NotFound;
	chalk = require('chalk');
	constructor(message: string) {
		super(message || 'The requested resource couldn\'t be found');
		this.stack = this.chalk.bold.keyword('orange')(this.stack);
	}
}
