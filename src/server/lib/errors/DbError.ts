/**
 * @file Contains DbError class that can be used to capture database errors.
 * @author Samuel Batista
 * {@link https://kostasbariotis.com/rest-api-error-handling-with-express-js/}
 */

export class DbError extends Error {
	name = 'DbError';
	chalk = require('chalk');
	constructor(message: string) {
		super(message || 'There was an unspecified database error');
		this.stack = this.chalk.bold.magenta(this.stack);
	}
}
