import { Client, Room, Delayed } from 'colyseus';
import { Schema, ArraySchema, MapSchema, type } from '@colyseus/schema';
import { GameModel } from '../../../models/GameModel';
import { PlayerModel } from '../../../models/PlayerModel';

class PlayerState extends Schema {
	@type('string')
	name: string;
	constructor(name: string) {
		super();
		this.name = name;
	}
}

class ChatRoomState extends Schema {
	@type('number')
	lifetime: number = 60; // seconds
	@type(['string'])
	messages = new ArraySchema<string>();
	@type({ map: PlayerState })
	players = new MapSchema<PlayerState>();

	createPlayer(id: string, name: string) {
		let player = this.players[id];
		if (!player) {
			player = new PlayerState(name);
			this.players[id] = player;
		}
		return player;
	}

	removePlayer(id: string) {
		delete this.players[ id ];
	}
}

export class ChatRoom extends Room<ChatRoomState> {
	tickTimer: Delayed;

	constructor() {
		super();
		this.setState(new ChatRoomState());
	}

	onInit(options: any) {
			console.log('ChatRoom created');
			this.tickTimer = this.clock.setInterval(this.tick, 1000);
	}

	onLeave(client: Client) {
			this.addMessage(`${this.state.players[client.id].name} has left`);
			this.state.removePlayer(client.id);
	}

	onMessage(client: Client, data: string | { type: string, id: number }) {
			console.log('ChatRoom received message from', client.id, ':', data);

			if (typeof data === 'string') {
				this.addMessage(`${this.state.players[client.id].name}: ${data}`);
				return;
			}

			switch (data.type) {
				case 'joined':
					PlayerModel.findOne(data.id).then(player => {
						this.state.createPlayer(client.id, player.firstName);
						this.addMessage(`${player.firstName} has joined`);
					});
					this.send(client, this.state.lifetime);
					break;
			}
	}

	addMessage(text: string) {
		const index = this.state.messages.length;
		this.state.messages.push(text);
		this.clock.setTimeout(() => this.state.messages.splice(index, 1), 36000);
		console.log(`message[${index}] = ${text}`);
	}

	onDispose() {
			console.log('Dispose ChatRoom' + this.roomName);
			GameModel.remove(parseInt(this.roomName));
	}

	tick = () => {
		console.log(`tick lifetime=${this.state.lifetime}`);
		this.broadcast(String(--this.state.lifetime));
		if (this.state.lifetime <= 0) {
			this.addMessage(`Room expired, it's now closed`);
			this.clock.setTimeout(() => this.disconnect(), 0);
			this.tickTimer.clear();
			this.lock();
			return;
		}
	}
}
