/**
 * @file Configures colyseus server.
 * @author Samuel Batista
 */
import { Express } from 'express';
import { createServer } from 'http';
import { Server, Room } from 'colyseus';
import { monitor } from '@colyseus/monitor';
import { ChatRoom } from './rooms/ChatRoom';
import { GameModel } from '../../models/GameModel';
import { COLYSEUS_PORT } from '../../../shared/config';

const port = process.env.PORT ? parseInt(process.env.PORT) : 80;
const host = process.env.HOST || 'localhost';
const protocol = process.env.PROTOCOL || 'http';
const isProd = process.env.NODE_ENV !== 'development';

export class Colyseus {
	static gameServer: Server;
	static rooms: {[key: string]: Room} = {};

	static initialize(express: Express) {
		this.gameServer = new Server({
			server: createServer(express)
		});

		this.gameServer.onShutdown(() => console.log(`game server is going down.`));

		this.gameServer.listen(COLYSEUS_PORT, null, null, (err: Error) => {
			if (err) {
				console.error(err);
				process.exit();
			} else {
				express.use('/colyseus', monitor(this.gameServer));
				console.log(`colyseus is running on ws://${host}:${COLYSEUS_PORT}`);
				console.log(`monitor colyseus on ${protocol}://${host}:${port}/colyseus`);

				// rehydrate rooms
				(async () => {
					const games = await GameModel.find();
					for (const game of games) {
						this.addRoom(game.id);
					}
				})();
			}
		});
		return this;
	}

	static addRoom(id: number) {
		console.log(`creating ChatRoom: ${id}`);
		this.gameServer.register(String(id), ChatRoom).then(handler =>
			handler.
				on('create', (room) => {
					console.log(`ChatRoom created: ${room.roomId}`);
					this.rooms[id] = room;
				}).
				on('dispose', (room) => console.log(`ChatRoom disposed : ${room.roomId}`)).
				on('join', (room, client) => console.log(`Client ${client.id} joined ChatRoom : ${room.roomId}`)).
				on('leave', (room, client) => console.log(`Client ${client.id} left ChatRoom : ${room.roomId}`))
		);
	}

	static removeRoom(id: number) {
		if (this.rooms[id]) {
			this.rooms[id].disconnect();
			delete this.rooms[id];
		} else {
			console.warn(`attempted to remove room for game with id '${id}, but failed to find it`);
		}
	}

	static removeAllRooms() {
		const roomKeys = Object.keys(this.rooms);
		for (let i = roomKeys.length - 1; i >= 0; --i) {
			this.rooms[roomKeys[i]].disconnect();
			delete this.rooms[roomKeys[i]];
		}
	}
}
