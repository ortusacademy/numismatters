/**
 * @file Configures express server.
 * @author Samuel Batista
 */
import { join, resolve } from 'path';
import { Express } from 'express';
import express = require('express');

// Configuration
const port = process.env.PORT ? parseInt(process.env.PORT) : 80;
const host = process.env.HOST || 'localhost';
const protocol = process.env.PROTOCOL || 'http';
const isProd = process.env.NODE_ENV !== 'development';
const staticDir = resolve(process.cwd(), './build/client');

// Set up Express server
const server: Express = express();

server.use(express.urlencoded({ extended: true }));
server.use(express.json());

function start() {
	// Serve static content
	server.use(express.static(staticDir));

	// Unhandled routes just return index - routing is done client side
	const index = join(staticDir, 'index.html');
	server.get('/*', function(req, res) {
		res.sendFile(index);
	});

	// Start the server
	server.listen(port, (err: Error) => {
		if (err) {
			console.error(err);
			process.exit();
		} else {
			console.log(`${process.env.NAME} is running on port ${port}`);

			// ParcelProxyServer hosts the front end on port 8080 and routes requests to server
			if (!isProd) {
				const path = require('path');
				const ParcelProxyServer = require('parcel-proxy-server');
				new ParcelProxyServer({
					entryPoint: path.resolve(process.cwd(), 'src/client/index.html'),
					parcelOptions: {
						detailedReport: true,
						logLevel: 3,
						outDir: staticDir,
						open: false,
						sourceMaps: true,
						target: 'browser',
						watch: true
					},
					proxies: {
						'/api': { target: `${protocol}://${host}:${port}` }
					}
				}).listen(8080, () => {
					console.log(`${process.env.NAME}: ${protocol}://${host}:${port}`);
				});
				console.log(`Parcel is building client, this might take a while...`);
			}
		}
	});
}

export { server, start };
