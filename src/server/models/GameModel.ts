import {
	Column,
	Entity,
	JoinTable,
	ManyToMany,
	OneToMany,
	getRepository,
	UpdateResult,
	DeleteResult,
	Repository,
	FindOneOptions,
	FindManyOptions,
	Index,
} from 'typeorm';
import { PlayerModel } from './PlayerModel';
import { BaseModel } from './BaseModel';
import { TransactionModel } from './TransactionModel';
import { IGameModel } from '../../shared/IGame';

@Entity({ name: 'game' })
export class GameModel extends BaseModel implements IGameModel {
	static repository: Repository<GameModel>;

	@Index()
	@Column()
	public name: string;

	@Column({ default: 40000 })
	public startingSalary: number = 40000;

	@Column({ default: 5 })
	public rounds: number = 5;

	@Column({ default: 1 })
	public difficulty: number = 1;

	@Column({ default: 0.05 })
	public debitFee: number = 0.05;

	@Column({ default: 0.05 })
	public creditFee: number = 0.05;

	@Column({ default: 0.15 })
	public lateFee: number = 0.15;

	// tslint:disable-next-line:no-shadowed-variable
	@ManyToMany(() => PlayerModel, (PlayerModel) => PlayerModel.games)
	@JoinTable({ name: 'game_players' })
	public players: PlayerModel[];

	// tslint:disable-next-line:no-shadowed-variable
	@OneToMany(() => TransactionModel, (TransactionModel) => TransactionModel.game)
	public transactions: TransactionModel[];

	static initialize() {
		this.repository = getRepository(GameModel);
	}

	static async find(options?: FindManyOptions<GameModel>): Promise<GameModel[]> {
		return this.repository.find(options);
	}

	static async findOne(id: string, options?: FindOneOptions<GameModel>): Promise<GameModel> {
		return this.repository.findOne(id, options);
	}

	static async findAndCount(options?: FindManyOptions<GameModel>): Promise<[GameModel[], number]> {
		return this.repository.findAndCount(options);
	}

	static async getCount(): Promise<number> {
		return this.repository.count();
	}

	static async create(data: Partial<GameModel>): Promise<GameModel> {
		return this.save(await this.repository.create(data));
	}

	static async update(data: GameModel): Promise<UpdateResult> {
		return this.repository.update(data.id, data);
	}

	static async save(data: GameModel): Promise<GameModel> {
		return this.repository.save(data);
	}

	static async remove(id: number): Promise<DeleteResult> {
		return this.repository.delete(id);
	}

	static async removeAll(): Promise<void> {
		return this.repository.clear();
	}
}
