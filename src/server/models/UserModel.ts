import { Entity, Column, Repository, getRepository, DeleteResult, UpdateResult } from 'typeorm';
import { BaseModel } from './BaseModel';

@Entity({ name: 'user' })
export class UserModel extends BaseModel {
	static repository: Repository<UserModel>;

	@Column()
	public firstName: string;

	@Column()
	public lastName: string;

	@Column()
	public age: number;

	static initialize() {
		this.repository = getRepository(UserModel);
	}

	static async find(): Promise<[UserModel[], number]> {
		return this.repository.findAndCount();
	}

	static async findOne(id: string): Promise<UserModel> {
		return this.repository.findOne(id);
	}

	static async getCount(): Promise<number> {
		return this.repository.count();
	}

	static async create(data: object /* Partial<IUserModel> */): Promise<UserModel> {
		const user = await this.repository.create(data);
		return this.repository.save(user);
	}

	static async update(data: any /* IUserModel */): Promise<UpdateResult> {
		return this.repository.update(data.id, data);
	}

	static async remove(id: string): Promise<DeleteResult> {
		return this.repository.delete(id);
	}

	static async removeAll(): Promise<void> {
		return this.repository.clear();
	}
}
