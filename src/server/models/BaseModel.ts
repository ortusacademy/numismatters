import {
	CreateDateColumn,
	UpdateDateColumn,
	PrimaryGeneratedColumn,
	Column,
	Generated,
} from 'typeorm';
import { IBaseModel } from '../../shared/IBaseModel';

export abstract class BaseModel implements IBaseModel {

	@PrimaryGeneratedColumn()
	public id: number;

	@Column()
	@Generated('uuid')
	public uuid: string;

	@CreateDateColumn()
	public createdAt: Date;

	@UpdateDateColumn()
	public updatedAt: Date;

}
