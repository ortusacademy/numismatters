import {
	Column,
	Entity,
	JoinTable,
	ManyToMany,
	OneToMany,
	Repository,
	getRepository,
	UpdateResult,
	DeleteResult,
	FindManyOptions,
	FindOneOptions
} from 'typeorm';
import { GameModel } from './GameModel';
import { BaseModel } from './BaseModel';
import { TransactionModel } from './TransactionModel';
import { IPlayerModel } from '../../shared/IPlayer';

@Entity({ name: 'player' })
export class PlayerModel extends BaseModel implements IPlayerModel {
	static repository: Repository<PlayerModel>;

	@Column()
	public firstName: string;

	@Column()
	public lastName: string;

	@Column()
	public grade: number;

	@Column()
	public age: number;

	// tslint:disable-next-line:no-shadowed-variable
	@ManyToMany(() => GameModel, (GameModel) => GameModel.players)
	public games: GameModel[];

	// tslint:disable-next-line:no-shadowed-variable
	@OneToMany(() => TransactionModel, (TransactionModel) => TransactionModel.player)
	@JoinTable({ name: 'player_transactions' })
	public transactions: TransactionModel[];

	static initialize() {
		PlayerModel.repository = getRepository(PlayerModel);
	}

	static async find(options?: FindManyOptions<PlayerModel>): Promise<PlayerModel[]> {
		return this.repository.find(options);
	}

	static async findOne(id: number, options?: FindOneOptions<PlayerModel>): Promise<PlayerModel> {
		return this.repository.findOne(id, options);
	}

	static async findAndCount(options?: FindManyOptions<PlayerModel>): Promise<[PlayerModel[], number]> {
		return this.repository.findAndCount(options);
	}

	static async getCount(): Promise<number> {
		return this.repository.count();
	}

	static async create(data: Partial<PlayerModel>): Promise<PlayerModel> {
		return this.save(await this.repository.create(data));
	}

	static async update(data: Partial<PlayerModel>): Promise<UpdateResult> {
		return this.repository.update(data.id, data);
	}

	static async save(data: PlayerModel): Promise<PlayerModel> {
		return this.repository.save(data);
	}

	static async remove(id: number): Promise<DeleteResult> {
		return this.repository.delete(id);
	}

	static async removeAll(): Promise<void> {
		return this.repository.clear();
	}
}
