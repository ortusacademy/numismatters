import {
	Column,
	Entity,
	ManyToOne,
	getRepository,
	UpdateResult,
	DeleteResult,
	Repository,
	FindManyOptions,
	FindOneOptions,
	Index,
} from 'typeorm';
import { GameModel } from './GameModel';
import { PlayerModel } from './PlayerModel';
import { BaseModel } from './BaseModel';
import { ITransactionModel } from '../../shared/ITransaction';
import { TransactionTypes } from '../../shared/TransactionTypes';
import { TransactionMethods } from '../../shared/TransactionMethods';

@Entity({ name: 'transaction' })
export class TransactionModel extends BaseModel implements ITransactionModel {
	static repository: Repository<TransactionModel>;

	@Column()
	public amount: number;

	@Column({ default: -1 })
	public round: number = -1;

	@Column({ default: 'Withdrawal' })
	public type: TransactionTypes = TransactionTypes.Withdrawal;

	@Column({ default: 'Cash' })
	public method: TransactionMethods = TransactionMethods.Cash;

	@Index()
	@ManyToOne(() => GameModel, (GameModel) => GameModel.transactions) // tslint:disable-line:no-shadowed-variable
	public game: GameModel;

	@Index()
	@ManyToOne(() => PlayerModel, (PlayerModel) => PlayerModel.transactions) // tslint:disable-line:no-shadowed-variable
	public player: PlayerModel;

	static initialize() {
		this.repository = getRepository(TransactionModel);
	}

	static async find(options?: FindManyOptions<TransactionModel>): Promise<TransactionModel[]> {
		return this.repository.find(options);
	}

	static async findOne(id: string, options?: FindOneOptions<TransactionModel>): Promise<TransactionModel> {
		return this.repository.findOne(id, options);
	}

	static async findAndCount(options?: FindManyOptions<TransactionModel>): Promise<[TransactionModel[], number]> {
		return this.repository.findAndCount(options);
	}

	static async getCount(): Promise<number> {
		return this.repository.count();
	}

	static async create(data: Partial<TransactionModel>): Promise<TransactionModel> {
		return this.save(await this.repository.create(data));
	}

	static async update(data: TransactionModel): Promise<UpdateResult> {
		return this.repository.update(data.id, data);
	}

	static async save(data: TransactionModel): Promise<TransactionModel> {
		return this.repository.save(data);
	}

	static async remove(id: string): Promise<DeleteResult> {
		return this.repository.delete(id);
	}

	static async removeAll(): Promise<void> {
		return this.repository.clear();
	}
}
