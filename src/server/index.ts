/**
 * @file Starts express REST server, and initializes sqlite3 db with typeorm
 * @author Samuel Batista
 */
import 'dotenv/config';
import 'reflect-metadata';
import 'source-map-support/register';
import { createConnection, Connection } from 'typeorm';
import { server, start } from './lib/ExpressServer';
import { Colyseus } from './lib/colyseus/Colyseus';
import { UserModel } from './models/UserModel';
import { GameModel } from './models/GameModel';
import { PlayerModel } from './models/PlayerModel';
import { TransactionModel } from './models/TransactionModel';
import { BankController } from './controllers/BankController';
import { GameController } from './controllers/GameController';
import { PlayerController } from './controllers/PlayerController';
import { RoundController } from './controllers/RoundController';

// load typeorm
export default createConnection({
	type: 'sqlite',
	database: process.env.DB_LOCATION || 'data/db.sqlite',
	synchronize: true, /* process.env.NODE_ENV === 'development' */
	entities: [
		GameModel,
		PlayerModel,
		TransactionModel,
		UserModel
	],
	logging: process.env.NODE_ENV === 'development'
}).then(async (db: Connection) => {

	// initialize models
	GameModel.initialize();
	PlayerModel.initialize();
	TransactionModel.initialize();
	UserModel.initialize();

	// setup web sockets
	Colyseus.initialize(server);

	// setup routes
	const baseUrl = process.env.API_URL || '/api';
	BankController(baseUrl, server);
	GameController(baseUrl, server);
	PlayerController(baseUrl, server);
	RoundController(baseUrl, server);

	// start server
	start();
});
