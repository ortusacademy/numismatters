/**
 * @file Sets up bank api endpoints
 * @author Samuel Batista
 */
import { Express } from 'express';
import { GameModel } from './../models/GameModel';
import { PlayerModel } from '../models/PlayerModel';
import { TransactionModel } from './../models/TransactionModel';
import { NotFound } from '../lib/errors/NotFound';
import { StatusCode } from '../../shared/StatusCode';
import { TransactionTypes } from '../../shared/TransactionTypes';
import { TransactionMethods } from '../../shared/TransactionMethods';
import { DbError } from '../lib/errors/DbError';

export const BankController = (baseUrl: string, http: Express) => {

	function getTransactions(gameId?: number, playerId?: number, method?: TransactionMethods) {
		const query = TransactionModel.repository.createQueryBuilder('transaction');
		if (gameId != null) { query.where('gameId = :gameId', { gameId }); }
		if (playerId != null) { query.where('playerId = :playerId', { playerId }); }
		if (method != null) { query.where('method = :method', { method }); }
		return query.getMany();
	}

	function sumTransactions(transactions: TransactionModel[]) {
		let total = 0;
		for (const transaction of transactions) {
			total += transaction.amount * (transaction.type === TransactionTypes.Withdrawal ? -1 : 1);
		}
		return total;
	}

	/**
	 * @api {get} Get the balance of a players debit account
	 * @returns {json} Integer sum of all transactions, representing the players current bank balance
	 */
	http.get(`${baseUrl}/bank/:gameId/cash/:playerId`, async (req, res, next) => {
		try {
			const transactions = await getTransactions(req.params.gameId, req.params.playerId, TransactionMethods.Cash);
			res.json(sumTransactions(transactions));
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get the balance of a players debit account
	 * @returns {json} Integer sum of all transactions, representing the players current bank balance
	 */
	http.get(`${baseUrl}/bank/:gameId/credit/:playerId`, async (req, res, next) => {
		try {
			const transactions = await getTransactions(req.params.gameId, req.params.playerId, TransactionMethods.Credit);
			res.json(sumTransactions(transactions));
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get the balance of a players debit account
	 * @returns {json} Integer sum of all transactions, representing the players current bank balance
	 */
	http.get(`${baseUrl}/bank/:gameId/debit/:playerId`, async (req, res, next) => {
		try {
			const transactions = await getTransactions(req.params.gameId, req.params.playerId, TransactionMethods.Debit);
			res.json(sumTransactions(transactions));
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {post} Deposit to a players bank.
	 * @returns {json} Transaction that deposits and status 201 if successful.
	 */
	http.post(`${baseUrl}/bank/:gameId/deposit/:playerId`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.gameId);
			if (game) {
				const player = await PlayerModel.findOne(req.params.playerId);
				if (player) {
					const { amount, method } = req.body;
					await TransactionModel.create({
						type: TransactionTypes.Deposit,
						game,
						player,
						amount: Math.abs(amount),
						method
					});
					res.json(sumTransactions(await getTransactions(game.id, player.id, method)));
					/* if (player.currentRound > 0) {
						res.status(201).json(TransactionModel.repository.create({
							type: TransactionTypes.Withdrawal,
							game,
							player,
							method,
							amount: -Math.abs(amount),
							round: player.currentRound
						}));
				} else { next(new HttpError(`Player with id ${req.params.playerId} has not claimed salary for first round`)); } */
				} else { next(new NotFound(`Failed to find player with id ${req.params.id}`)); }
			} else { next(new NotFound(`Failed to find game with id ${req.params.id}`)); }
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {post} Withdrawal from a players bank.
	 * @returns {json} Transaction that withdrawals and 201 if successful.
	 */
	http.post(`${baseUrl}/bank/:gameId/withdrawal/:playerId`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.gameId);
			if (game) {
				const player = await PlayerModel.findOne(req.params.playerId);
				if (player) {
					const { amount, method } = req.body;
					await TransactionModel.create({
						type: TransactionTypes.Withdrawal,
						game,
						player,
						amount: Math.abs(amount),
						method
					});
					res.json(sumTransactions(await getTransactions(game.id, player.id, method)));
				} else { next(new NotFound(`Failed to find player with id ${req.params.playerId}`)); }
			} else {
				next(new NotFound(`Failed to find game with id ${req.params.gameId}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {put} Updates a transaction by id.
	 * @returns {json} A transaction if found, or null.
	 */
	http.put(`${baseUrl}/bank/update/:id`, async (req, res, next) => {
		try {
			await TransactionModel.update(req.body);
			res.sendStatus(StatusCode.OK);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all transactions.
	 * @returns {json} An array with all transactions.
	 */
	http.get(`${baseUrl}/bank/history`, async (req, res, next) => {
		try {
			res.json(await getTransactions(null, null, req.body.method));
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all transactions from a game.
	 * @returns {json} An array with transactions for the specified game.
	 */
	http.get(`${baseUrl}/bank/:gameId/history`, async (req, res, next) => {
		try {
			res.json(await getTransactions(req.params.gameId, req.body.method));
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all transactions from a player.
	 * @returns {json} An array with transactions for the specified player.
	 */
	http.get(`${baseUrl}/bank/history/:playerId`, async (req, res, next) => {
		try {
			res.json(await getTransactions(null, req.params.playerId, req.body.method));
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all transactions from a game and player.
	 * @returns {json} An array with transactions for the specified player and game.
	 */
	http.get(`${baseUrl}/bank/:gameId/history/:playerId`, async (req, res, next) => {
		try {
			res.json(await getTransactions(req.params.gameId, req.params.playerId, req.body.method));
		} catch (error) {
			next(new DbError(error));
		}
	});
};
