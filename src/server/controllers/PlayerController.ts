/**
 * @file Sets up player api endpoints
 * @author Samuel Batista
 */
import { Express } from 'express';
import { PlayerModel } from '../models/PlayerModel';
import { DbError } from '../lib/errors/DbError';
import { NotFound } from '../lib/errors/NotFound';
import { StatusCode } from '../../shared/StatusCode';

export const PlayerController = (baseUrl: string, http: Express) => {

	/**
	 * @api {post} Create a new player.
	 * @returns {json} The created player.
	 */
	http.post(`${baseUrl}/player`, async (req, res, next) => {
		try {
			const player = await PlayerModel.create(req.body);
			res.json(player);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all players
	 * @returns {json} An array with all players.
	 */
	http.get(`${baseUrl}/player/all`, async (req, res, next) => {
		try {
			const [data, count] = await PlayerModel.findAndCount();
			res.json(count > 0 && data || []);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get a player by id.
	 * @returns {json} A player if found, or null.
	 */
	http.get(`${baseUrl}/player/:id`, async (req, res, next) => {
		try {
			const player = await PlayerModel.findOne(req.params.id);
			if (player) {
				res.json(player);
			} else {
				next(new NotFound(`Failed to find player with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {put} Updates a player by id.
	 * @returns {json} A player if found, or null.
	 */
	http.put(`${baseUrl}/player/:id`, async (req, res, next) => {
		try {
			const player = await PlayerModel.findOne(req.params.id);
			if (player) {
				await PlayerModel.update({ ...req.body, ...{ id: req.params.id } });
				res.sendStatus(StatusCode.OK);
			} else {
				next(new NotFound(`Failed to find player with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get a player`s round by id.
	 * @returns {json} Players current round if found, or null.
	 */
	http.get(`${baseUrl}/player/:id/round/`, async (req, res, next) => {
		try {
			// TODO
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {delete} Remove all players.
	 * @returns {json} Number of games removed.
	 */
	http.delete(`${baseUrl}/player/all`, async (req, res, next) => {
		try {
			await PlayerModel.removeAll();
			res.sendStatus(StatusCode.OK);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {delete} Remove a player by id.
	 * @returns {json} An array of remaining players.
	 */
	http.delete(`${baseUrl}/player/:id/remove`, async (req, res, next) => {
		try {
			const player = await PlayerModel.findOne(req.params.id);
			if (player) {
				await PlayerModel.remove(player.id);
				res.sendStatus(StatusCode.OK);
			} else {
				next(new NotFound(`Failed to find player with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

};
