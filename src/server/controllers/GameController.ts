/**
 * @file Sets up game api endpoints
 * @author Samuel Batista
 */
import { Express } from 'express';
import { GameModel } from '../models/GameModel';
import { PlayerModel } from '../models/PlayerModel';
import { NotFound } from '../lib/errors/NotFound';
import { StatusCode } from '../../shared/StatusCode';
import { DbError } from '../lib/errors/DbError';
import { Colyseus } from '../lib/colyseus/Colyseus';

export const GameController = (baseUrl: string, http: Express) => {

	/**
	 * @api {post} Create a game and return the default parameters.
	 * @returns {json} A new game with default values.
	 */
	http.get(`${baseUrl}/game/default`, async (req, res, next) => {
		res.json(new GameModel());
	});

	/**
	 * @api {post} Create a new game.
	 * @returns {json} An array with all games.
	 */
	http.post(`${baseUrl}/game`, async (req, res, next) => {
		try {
			const game = await GameModel.create(req.body);
			await Colyseus.addRoom(game.id);
			res.json(game);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all games
	 * @returns {json} An array with all games.
	 */
	http.get(`${baseUrl}/game/all`, async (req, res, next) => {
		try {
			const [data, count] = await GameModel.findAndCount();
			res.json(count > 0 && data || []);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get a game by id.
	 * @returns {json} An array with all games.
	 */
	http.get(`${baseUrl}/game/:id`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.id);
			if (game) {
				res.json(game);
			} else {
				next(new NotFound(`Failed to find game with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {put} Updates a game by id.
	 * @returns {json} A game if found, or null.
	 */
	http.put(`${baseUrl}/game/:id/update`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.id);
			if (game) {
				await GameModel.update({ ...req.body, ...{ id: req.params.id } });
				res.sendStatus(StatusCode.OK);
			} else {
				next(new NotFound(`Failed to find game with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {get} Get all players in a game.
	 * @returns {json} An array with all players.
	 */
	http.get(`${baseUrl}/game/:id/players`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.id);
			if (game) {
				res.json(game.players);
			} else {
				next(new NotFound(`Failed to find game with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {put} Add a player to a game
	 * @returns {json} An array with game accessed.
	 */
	http.put(`${baseUrl}/game/:id/player/:playerId`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.id, { relations: [`players`] });
			if (game) {
				const player = await PlayerModel.findOne(req.params.playerId);
				if (player) {
					if (!game.players.find((p) => p.id === player.id)) {
						game.players.push(player);
						await GameModel.save(game);
					}
					res.json(game);
				} else { next(new NotFound(`Failed to find player with id ${req.params.id}`)); }
			} else { next(new NotFound(`Failed to find game with id ${req.params.id}`)); }
		} catch (error) { next(new DbError(error)); }
	});

	/**
	 * @api {put} Adds all players to a game
	 * @returns {json} An array with game accessed.
	 */
	http.put(`${baseUrl}/game/:id/player/all`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.id, { relations: [`players`] });
			if (game) {
				const players = await PlayerModel.find();
				if (players) {
					let changed = false;
					for (const player of players) {
						if (!game.players.find((p) => p.id === player.id)) {
							game.players.push(player);
							changed = true;
						}
					}
					if (changed) {
						await GameModel.save(game);
					}
					res.json(game);
				}
			} else { next(new NotFound(`Failed to find game with id ${req.params.id}`)); }
		} catch (error) { next(new DbError(error)); }
	});

	/**
	 * @api {delete} Remove all games.
	 * @returns {json} Number of games removed.
	 */
	http.delete(`${baseUrl}/game/all`, async (req, res, next) => {
		try {
			await GameModel.removeAll();
			await Colyseus.removeAllRooms();
			res.sendStatus(StatusCode.OK);
		} catch (error) {
			next(new DbError(error));
		}
	});

	/**
	 * @api {delete} Remove a game by id.
	 * @returns {json} An array of remaining games.
	 */
	http.delete(`${baseUrl}/game/:id/remove`, async (req, res, next) => {
		try {
			const game = await GameModel.findOne(req.params.id);
			if (game) {
				await GameModel.remove(game.id);
				Colyseus.removeRoom(game.id);
				res.sendStatus(StatusCode.OK);
			} else {
				next(new NotFound(`Failed to find game with id ${req.params.id}`));
			}
		} catch (error) {
			next(new DbError(error));
		}
	});

};
