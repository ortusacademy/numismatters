export function isCancel(command) {
	switch (command.toLowerCase()) {
		case '.': return true;
		case 'cancel': return true;
		case 'exit': return true;
		case 'back': return true;
		case 'go back': return true;
	}
	return false;
}
