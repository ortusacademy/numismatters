import * as React from 'react';
import { render } from 'react-dom';
import { App } from './components/app/App';

import './styles/styles.scss';

render((<App />), document.getElementById('root'));
