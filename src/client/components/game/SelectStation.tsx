import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Paper, List, ListItem, Grid, Button, withStyles, createStyles, WithStyles } from '@material-ui/core';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%'
	},
	list: {},
});

interface IProps extends RouteComponentProps<any>, Partial<WithStyles<typeof styles>> {}

export const SelectStation = withRouter<IProps>(withStyles(styles)(
	class extends React.Component<IProps, any> {
		public render() {
			const { classes } = this.props;
			return (<Paper className={classes.container}>
			<Grid container spacing={16} direction='column'>
				<List className={classes.list}>
					{this.buttonItem('/lobby', 'Lobby')}
					{this.buttonItem('/bank', 'Bank')}
					{this.buttonItem('/bills', 'Bills')}
					{this.buttonItem('/fun', 'Fun')}
					{this.buttonItem('/investments', 'Investments')}
					{this.buttonItem('/lottery', 'Lottery')}
					{this.buttonItem('/real-estate', 'Real Estate')}
				</List>
			</Grid>
		</Paper>
			);
		}

		buttonItem = (url: string, text: string) => {
			const { pathname } = this.props.location;
			return <ListItem>
				<Button
					variant='text'
					className={this.props.classes.centered}
					onClick={() => this.props.history.push(pathname.replace('/home', url))}
				>
					{text}
				</Button>
			</ListItem>;
		}
	}
));
