import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Paper, createStyles, withStyles, WithStyles } from '@material-ui/core';
import { Loading } from '../app/Loading';
import { GameService } from '../../services/GameService';
import { IGameModel, IGame } from '../../../shared/IGame';
import { EditOrCreateGame } from './EditOrCreateGame';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%'
	},
	list: {},
});

type IProps = RouteComponentProps<any> & Partial<WithStyles<typeof styles>>;

interface IState {
	game?: IGame | IGameModel;
	gameToRemove?: IGameModel;
	error?: string;
}

export const EditOrCreateGameContainer = withRouter<IProps>(withStyles(styles)(
	class EditOrCreateGameContainer extends React.Component<IProps, IState> {
		constructor(props: IProps) {
			super(props);
			this.state = {
				game: null,
				gameToRemove: null,
				error: null,
			};
			this.fetchGame();
		}

		public render() {
			if (this.state.error) return this.state.error;
			if (!this.state.game) return <Loading />;

			const { classes } = this.props;

			return <>
				<Paper className={classes.container}>
					<EditOrCreateGame
						game={this.state.game}
						onRemove={this.onRemove}
						onUpdated={this.onUpdated}
					/>
				</Paper>
				<Dialog
					open={!!this.state.gameToRemove}
					onClose={this.onCancelRemoveGame}
					aria-labelledby='alert-dialog-title'
					aria-describedby='alert-dialog-description'
				>
					<DialogTitle id='alert-dialog-title'>Are you sure?</DialogTitle>
					<DialogContent>
						<DialogContentText id='alert-dialog-description'>
							Are you sure you want to remove {this.state.gameToRemove && this.state.gameToRemove.name}?
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.onConfirmRemoveGame} color='secondary'>
							Confirm
						</Button>
						<Button onClick={this.onCancelRemoveGame} color='primary' autoFocus>
							Cancel
						</Button>
					</DialogActions>
				</Dialog>
			</>;
		}

		onUpdated = (game: IGameModel) => this.setState({ game });
		onRemove = (gameToRemove: IGameModel) => this.setState({ gameToRemove });

		private onCancelRemoveGame = () => this.setState({ gameToRemove: null });

		private onConfirmRemoveGame = () => {
			const removeId = this.state.gameToRemove && this.state.gameToRemove.id;
			this.setState({ gameToRemove: null, game: null });
			GameService.removeGame(removeId, null, error => this.setState({ error: error.message }));
		}

		private fetchGame() {
			if (this.props.match.params && this.props.match.params.gameId) {
				GameService.getGame(this.props.match.params.gameId, game => this.setState({ game }), error => this.setState({ error: error.message }));
			} else {
				GameService.getDefaultGame(game => this.setState({ game }), error => this.setState({ error: error.message }));
			}
		}

	}
) as any);
