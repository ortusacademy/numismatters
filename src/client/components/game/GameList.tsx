import * as React from 'react';
import update from 'immutability-helper';
import { createStyles, WithStyles, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Loading } from '../app/Loading';
import { GameListItem } from './GameListItem';
import { GameService } from '../../services/GameService';
import { IGameModel } from '../../../shared/IGame';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%'
	},
	list: {},
});

type IProps = RouteComponentProps<any> & Partial<WithStyles<typeof styles>>;

interface IState {
	error?: string;
	isLoading: boolean;
	disableAdd: boolean;
	games: IGameModel[];
	gameToRemove?: IGameModel;
}

export const GameList = withRouter<IProps>(withStyles(styles)(
	class extends React.Component<IProps, IState> {
		state = {
			disableAdd: false,
			error: null,
			games: [],
			gameToRemove: null,
			isLoading: true
		};

		componentDidMount() {
			this.fetch();
		}

		public render() {
			const { error, isLoading, games, gameToRemove, disableAdd } = this.state;
			if (error) return error;
			if (isLoading) return <Loading />;

			const { classes } = this.props;
			const hasGames = games.length > 0;

			return <>
				<Paper className={classes.container}>
					<Grid container spacing={16} direction='column'>
						<List className={classes.list} component='nav'>
							{hasGames && games.map((game, index) =>
								<GameListItem key={index} game={game} onUpdated={this.onGameUpdated} onRemove={this.onRemoveGame}/>
							)}
							{!hasGames &&
								<ListItem>
									<ListItemText
										className={classes.centered}
										inset primary={`You have no rooms, click 'Add Room' bellow to get started.`}
									/>
								</ListItem>
							}
							<ListItem>
								<Button variant='text' className={classes.centered} onClick={this.onAddGame} disabled={disableAdd}>
									Add Room
								</Button>
							</ListItem>
						</List>
					</Grid>
				</Paper>
				<Dialog
						open={gameToRemove != null}
						onClose={this.onCancelRemoveGame}
						aria-labelledby='alert-dialog-title'
						aria-describedby='alert-dialog-description'
					>
						<DialogTitle id='alert-dialog-title'>Are you sure?</DialogTitle>
						<DialogContent>
							<DialogContentText id='alert-dialog-description'>
								Are you sure you want to remove {gameToRemove && gameToRemove.name}?
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button onClick={this.onConfirmRemoveGame} color='secondary'>
								Confirm
							</Button>
							<Button onClick={this.onCancelRemoveGame} color='primary' autoFocus>
								Cancel
							</Button>
						</DialogActions>
					</Dialog>
				</>;
		}

		private onAddGame = (e: React.MouseEvent<HTMLButtonElement>) => {
			this.setState({ disableAdd: true });
			this.props.history.push('/game/create');
		}

		private onRemoveGame = (game: IGameModel) => {
			this.setState(update(this.state, { gameToRemove: { $set: game } }));
		}

		private onGameUpdated = (game: IGameModel) => {
			this.setState(update(this.state, {
				games: {
					$apply: (prevGames: IGameModel[]) =>
						prevGames.map((prevGame) => prevGame.id !== game.id ? prevGame : game)
				}
			}));
		}

		private onConfirmRemoveGame = () => {
			const removeId = this.state.gameToRemove && this.state.gameToRemove.id;
			this.setState(update(this.state, {
				gameToRemove: { $set: null },
				games: {
					$apply: (prevGames: IGameModel[]) =>
					prevGames.filter((prevGame) => prevGame.id !== removeId)
				}
			}));
			GameService.removeGame(removeId, null, (error) => this.setState({ error: error.message }));
		}

		private onCancelRemoveGame = () => {
			this.setState(update(this.state, { gameToRemove: { $set: null } }));
		}

		private fetch = () => {
			this.setState({ isLoading: true });
			GameService.getGames(
				(games) => this.setState({ games, isLoading: false, disableAdd: false }),
				(error) => this.setState({ error: error.message }),
			);
		}
	}
) as any);
