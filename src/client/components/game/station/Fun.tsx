import * as React from 'react';
import { Loading } from '../../app/Loading';

interface IState {
	error?: any;
	data?: any;
}
export class Fun extends React.Component<any, IState> {
	constructor(props) {
		super(props);
		this.state = {
			data: null,
			error: null
		};
	}

	public render() {
		if (this.state.error) return this.state.error;
		if (!this.state.data) return <Loading />;
	}
}
