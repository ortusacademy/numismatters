import * as React from 'react';
import { createStyles, WithStyles } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { IGameModel } from '../../../shared/IGame';
import { EditOrCreateGame } from './EditOrCreateGame';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4,
	},
});

interface IProps extends RouteComponentProps<any>, Partial<WithStyles<typeof styles>> {
	game: IGameModel;
	onUpdated: (game: IGameModel) => void;
	onRemove: (game: IGameModel) => void;
}

interface IState {
	isOpen: boolean;
}

export const GameListItem = withRouter<IProps>(withStyles(styles)(
	class GameListItem extends React.Component<IProps, IState> {
		state: IState = {
			isOpen: true
		};

		componentWillReceiveProps(nextProps) {
			if (this.props.game.id !== nextProps.game.id) {
				this.setState({ isOpen: false });
			}
		}

		public render() {
			const { game } = this.props;
			return (
				<>
					<ListItem button onClick={this.onClick}>
						<ListItemText inset primary={game.name} />
						{this.state.isOpen ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse in={this.state.isOpen} timeout='auto' unmountOnExit>
						<List disablePadding>
							<ListItem>
								<EditOrCreateGame game={game} onUpdated={this.props.onUpdated} onRemove={this.props.onRemove}/>
							</ListItem>
						</List>
					</Collapse>
				</>
			);
		}

		private onClick = () => {
			this.setState((state) => ({ isOpen: !state.isOpen }));
		}

	}
) as any);
