import * as React from 'react';
import update from 'immutability-helper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import { createStyles, WithStyles } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Loading } from '../app/Loading';
import { IGame, IGameModel } from '../../../shared/IGame';
import { GameService } from '../../services/GameService';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%',
	},
	list: {}
});

interface IProps extends RouteComponentProps<any>, Partial<WithStyles<typeof styles>> {
	game: IGame | IGameModel;
	onRemove?: (game: IGameModel) => void;
	onUpdated?: (game: IGameModel) => void;
}

interface IState {
	disableSave: boolean;
	hasChanges: boolean;
}

export const EditOrCreateGame = withRouter<IProps>(withStyles(styles)(
	class EditOrCreateGame extends React.Component<IProps, IState> {
		private get game() { return this.props.game as IGameModel; }

		constructor(props: IProps) {
			super(props);
			this.state = {
				hasChanges: false,
				disableSave: !this.isValidGame(props.game)
			};
		}

		componentWillReceiveProps(nextProps) {
			this.setState((prevState) => this.updateGame(prevState, nextProps.game, true));
		}

		public render() {
			const { classes, game } = this.props;
			if (!game) return <Loading />;
			const isExisting = game && game.hasOwnProperty('id');

			return (
				<Paper className={classes.container}>
					<Grid container spacing={16} direction='column'>
						<List className={classes.list} component='nav'>
						<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='name'>Name</InputLabel>
									<Input id='name' value={game.name} onChange={this.onNameChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='difficulty'>Difficulty</InputLabel>
									<Input id='difficulty' value={game.difficulty} onChange={this.onDifficultyChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='number-rounds'>Number of Rounds</InputLabel>
									<Input id='number-rounds' value={game.rounds} onChange={this.onRoundsChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='starting-salary'>Starting Salary</InputLabel>
									<Input id='starting-salary' value={game.startingSalary} onChange={this.onSalaryChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='debit-fee'>Debit Fee</InputLabel>
									<Input id='debit-fee' value={game.debitFee} onChange={this.onDebitFeeChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='credit-fee'>Credit Fee</InputLabel>
									<Input id='credit-fee' value={game.debitFee} onChange={this.onCreditFeeChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='late-fee'>Late Fee</InputLabel>
									<Input id='late-fee' value={game.debitFee} onChange={this.onLateFeeChanged} />
								</FormControl>
							</ListItem>
							{isExisting ?
							<>
								<ListItem>
									<Button variant='text' className={classes.centered} onClick={this.onSubmit}>
										{this.state.hasChanges ? 'Save' : 'Enter'}
									</Button>
								</ListItem>
								<ListItem>
									<Button variant='text' color='secondary' className={classes.centered} onClick={this.onRemove}>
										Remove
									</Button>
								</ListItem>
							</>
							: <>
								<ListItem>
									<Button variant='text' className={classes.centered} onClick={this.onSubmit} disabled={this.state.disableSave}>
										Create
									</Button>
								</ListItem>
								<ListItem>
									<Button variant='text' className={classes.centered} onClick={this.onBack}>
										Back
									</Button>
								</ListItem>
							</>}
						</List>
					</Grid>
				</Paper>
			);
		}

		private onNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { name: { $set: String(e.currentTarget.value) } }) as IGameModel);
		}

		private onDifficultyChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { difficulty: { $set: Number(e.currentTarget.value) } }) as IGameModel);
		}

		private onSalaryChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { startingSalary: { $set: Number(e.currentTarget.value) } }) as IGameModel);
		}

		private onRoundsChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { rounds: { $set: Number(e.currentTarget.value) } }) as IGameModel);
		}

		private onDebitFeeChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { debitFee: { $set: Number(e.currentTarget.value) } }) as IGameModel);
		}

		private onCreditFeeChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { creditFee: { $set: Number(e.currentTarget.value) } }) as IGameModel);
		}

		private onLateFeeChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
			this.props.onUpdated(update(this.props.game, { lateFee: { $set: Number(e.currentTarget.value) } }) as IGameModel);
		}

		private onSubmit = () => {
			const { game } = this.props;
			const isExisting = game && game.hasOwnProperty('id');

			if (isExisting) {
				if (this.state.hasChanges) {
					this.setState({ disableSave: true });
					GameService.updateGame(this.game, () => alert('Success'), (error) => console.error(error));
				} else {
					this.props.history.push(`/game/${(this.game).id}/start`);
				}
			} else {
				this.setState({ disableSave: true });
				GameService.addGame(this.game,
					() => this.props.history.push('/games'),
					(error) => console.error(error));
			}
		}

		private onRemove = () => {
			if (this.props.onRemove && this.props.game && this.props.game.hasOwnProperty('id')) {
				this.props.onRemove(this.game);
			}
		}

		private onBack = () => {
			this.props.history.goBack();
		}

		private isValidGame(game: IGame) {
			return game
				&& !!game.name
				&& game.difficulty > 0
				&& game.rounds > 0
				&& game.startingSalary > 0;
		}

		private updateGame = (prevState: IState, updatedGame: IGame | IGameModel, resetChanges: boolean) => update(prevState, {
			disableSave: { $set: !this.isValidGame(updatedGame) },
			hasChanges: { $set: !resetChanges }
		});

	}
) as any);
