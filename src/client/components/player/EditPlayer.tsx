import * as React from 'react';
import update from 'immutability-helper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import { createStyles, WithStyles, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Loading } from '../app/Loading';
import { IPlayer, IPlayerModel } from '../../../shared/IPlayer';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%',
	},
	list: {}
});

interface IProps extends RouteComponentProps<any>, Partial<WithStyles<typeof styles>> {
	player: IPlayer | IPlayerModel;
	onSubmit: (player: IPlayer | IPlayerModel) => void;
	submitText?: string;
}

interface IState {
	disableSave: boolean;
	error?: string;
	isSaving: boolean;
	pendingPlayer: IPlayer;
}

export const EditPlayer = withRouter<IProps>(withStyles(styles)(
	class extends React.Component<IProps, IState> {
		constructor(props: IProps) {
			super(props);
			this.state = {
				error: null,
				isSaving: false,
				pendingPlayer: { ...props.player },
				disableSave: !this.isValidPlayer(props.player)
			};
		}

		componentWillReceiveProps(nextProps) {
			this.setState((prevState) => update(prevState, {
				pendingPlayer: { $merge: nextProps.player },
				disableSave: { $set: !this.isValidPlayer(nextProps.player) }
			}));
		}

		public render() {
			if (this.state.error) return this.state.error;
			if (this.state.isSaving) return <Loading />;

			const { classes } = this.props;
			const { pendingPlayer } = this.state;

			return (
				<Paper className={classes.container}>
					<Grid container spacing={16} direction='column'>
						<List className={classes.list} component='nav'>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='firstName'>First Name</InputLabel>
									<Input id='firstName' value={pendingPlayer.firstName} onChange={this.onFirstNameChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<InputLabel htmlFor='lastName'>Last Name</InputLabel>
									<Input id='lastName' value={pendingPlayer.lastName} onChange={this.onLastNameChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<TextField id='age' helperText={'Age'} type='number' value={pendingPlayer.age} onChange={this.onAgeChanged} />
								</FormControl>
							</ListItem>
							<ListItem className={classes.centered}>
								<FormControl>
									<TextField id='grade' helperText={'Grade'} type='number' value={pendingPlayer.grade} onChange={this.onGradeChanged} />
								</FormControl>
							</ListItem>
							<ListItem>
								<Button variant='text' className={classes.centered} onClick={this.onSubmit} disabled={this.state.disableSave}>
									{this.props.submitText || 'Submit'}
								</Button>
							</ListItem>
							<ListItem>
								<Button variant='text' className={classes.centered} onClick={this.onBack}>
									Back
								</Button>
							</ListItem>
						</List>
					</Grid>
				</Paper>
			);
		}

		private onFirstNameChanged = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
			this.validateState(update(this.state, { pendingPlayer: { firstName: { $set: String(event.currentTarget.value) } } }));
		}

		private onLastNameChanged = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
			this.validateState(update(this.state, { pendingPlayer: { lastName: { $set: String(event.currentTarget.value) } } }));
		};

		private onAgeChanged = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
			this.validateState(update(this.state, { pendingPlayer: { age: { $apply: (_) => this.greaterThanZero(Number(event.currentTarget.value)) } } } ));
		};

		private onGradeChanged = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
			this.validateState(update(this.state, { pendingPlayer: { grade: { $apply: (_) => this.greaterThanZero(Number(event.currentTarget.value)) } } }));
		};

		private onSubmit = () => {
			this.setState({ isSaving: true, disableSave: true });
			this.props.onSubmit(this.state.pendingPlayer);
		};

		private onBack = () => {
			this.props.history.goBack();
		};

		private isValidPlayer(player: IPlayer) {
			return player && !!player.firstName && !!player.lastName;
		}

		private validateState(state: IState) {
			this.setState(update(state, { disableSave: { $set: !this.isValidPlayer(state.pendingPlayer) }}));
		}

		private greaterThanZero(num: number) {
			return num > 0 ? num : 1;
		}

	}
) as any);
