import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Button, createStyles, WithStyles, withStyles } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import { IPlayerModel } from '../../../shared/IPlayer';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	}
});

interface IProps extends RouteComponentProps<any>, Partial<WithStyles<typeof styles>> {
	player: IPlayerModel;
	onSelected: (player: IPlayerModel) => void;
}

export const SelectPlayerListItem = withRouter<IProps>(withStyles(styles)(
	class extends React.Component<IProps, any> {
		public render() {
			const { player } = this.props;
			return (
				<ListItem>
					<Button
						variant='text'
						className={this.props.classes.centered}
						onClick={this.onClick}
					>
						{player.firstName} {player.lastName}
					</Button>
			</ListItem>
			);
		}
		private onClick = (e) => {
			this.props.onSelected(this.props.player);
		}
}));
