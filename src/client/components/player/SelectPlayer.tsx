import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { createStyles, WithStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import { Loading } from '../app/Loading';
import { PlayerService } from '../../services/PlayerService';
import { IPlayerModel } from '../../../shared/IPlayer';
import { SelectPlayerListItem } from './SelectPlayerListItem';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%'
	},
	list: {},
});

type IProps = RouteComponentProps<any> & Partial<WithStyles<typeof styles>>;

interface IState {
	error?: string;
	isLoading: boolean;
	disableAdd: boolean;
	players: IPlayerModel[];
	playersToAdd: IPlayerModel[];
}

export const SelectPlayer = withRouter(withStyles(styles)(
	class extends React.Component<IProps, IState> {
		state: IState = {
			disableAdd: false,
			error: null,
			isLoading: true,
			players: [],
			playersToAdd: []
		};

		componentDidMount() {
			this.fetch();
		}

		public render() {
			const { error, isLoading, players, disableAdd } = this.state;
			if (error) return error;
			if (isLoading) return <Loading />;

			const { classes } = this.props;
			const hasPlayers = players.length > 0;

			return (
				<Paper className={classes.container}>
					<Grid container spacing={16} direction='column'>
						<List className={classes.list} component='nav'>
							{hasPlayers && players.map((player, index) =>
								<SelectPlayerListItem key={index} player={player} onSelected={this.onSelectPlayer} />
							)}
							{!hasPlayers &&
								<ListItem>
									<ListItemText
										className={classes.centered}
										inset primary={`You have no players, click 'Add Player' bellow to get started.`}
									/>
								</ListItem>
							}
							<ListItem>
								<Button variant='text' className={classes.centered} onClick={this.onAddPlayer} disabled={disableAdd}>
									Add Player
							</Button>
							</ListItem>
						</List>
					</Grid>
				</Paper>
			);
		}

		private onAddPlayer = (e: React.MouseEvent<HTMLButtonElement>) => {
			this.props.history.push('/player/create');
		}

		private onSelectPlayer = (player: IPlayerModel) => {
			this.props.history.push(`/home/${this.props.match.params.gameId}/${player.id}`);
		}

		private fetch() {
			this.setState({ isLoading: true, disableAdd: true });
			PlayerService.getPlayers(
				(players) => this.setState({ players, isLoading: false, disableAdd: false }),
				(error) => this.setState({ isLoading: false, error: error.message }),
			);
		}
	}
));
