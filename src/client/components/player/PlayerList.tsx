import * as React from 'react';
import update from 'immutability-helper';
import { RouteComponentProps, withRouter } from 'react-router';
import { createStyles, WithStyles, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import { Loading } from '../app/Loading';
import { PlayerService } from '../../services/PlayerService';
import { IPlayerModel } from '../../../shared/IPlayer';
import { PlayerListItem } from './PlayerListItem';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	container: {
		backgroundColor: theme.palette.background.paper,
		marginTop: '15px',
		width: '100%'
	},
	list: {},
});

type IProps = RouteComponentProps<any> & Partial<WithStyles<typeof styles>>;

interface IState {
	error?: string;
	isLoading: boolean;
	disableAdd: boolean;
	players: IPlayerModel[];
	playerToRemove?: IPlayerModel;
}

export const PlayerList = withRouter(withStyles(styles)(
	class extends React.Component<IProps, IState> {
		state: IState = {
			disableAdd: false,
			error: null,
			isLoading: true,
			players: [],
			playerToRemove: null
		};

		componentDidMount() {
			this.fetch();
		}

		public render() {
			const { error, isLoading, players, playerToRemove, disableAdd } = this.state;
			if (error) return error;
			if (isLoading) return <Loading />;

			const { classes } = this.props;
			const hasPlayers = players.length > 0;

			return (
				<>
				<Paper className={classes.container}>
					<Grid container spacing={16} direction='column'>
						<List className={classes.list} component='nav'>
							{hasPlayers && players.map((player, index) =>
								<PlayerListItem key={index} player={player} onRemove={this.onRemovePlayer} />
							)}
							{!hasPlayers &&
								<ListItem>
									<ListItemText
										className={classes.centered}
										inset primary={`You have no players, click 'Add Player' bellow to get started.`}
									/>
								</ListItem>
							}
							<ListItem>
								<Button variant='text' className={classes.centered} onClick={this.onAddPlayer} disabled={disableAdd}>
									Add Player
							</Button>
							</ListItem>
						</List>
					</Grid>
				</Paper>
				<Dialog
				open={playerToRemove != null}
				onClose={this.onCancelRemovePlayer}
				aria-labelledby='alert-dialog-title'
				aria-describedby='alert-dialog-description'
			>
				<DialogTitle id='alert-dialog-title'>Are you sure?</DialogTitle>
				<DialogContent>
					<DialogContentText id='alert-dialog-description'>
						Are you sure you want to remove {playerToRemove && `${playerToRemove.firstName} ${playerToRemove.lastName}`}?
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.onConfirmRemovePlayer} color='secondary'>
						Confirm
					</Button>
					<Button onClick={this.onCancelRemovePlayer} color='primary' autoFocus>
						Cancel
					</Button>
				</DialogActions>
			</Dialog>
		</>
			);
		}

		private onAddPlayer = (e: React.MouseEvent<HTMLButtonElement>) => {
			this.props.history.push('/player/create');
		}

		private onRemovePlayer = (player: IPlayerModel) => {
			this.setState(update(this.state, { playerToRemove: { $set: player } }));
		}

		private onConfirmRemovePlayer = () => {
			const removeId = this.state.playerToRemove && this.state.playerToRemove.id;
			this.setState(update(this.state, {
					playerToRemove: { $set: null },
				players: {
					$apply: (prevPlayers: IPlayerModel[]) =>
					prevPlayers.filter((prevPlayer) => prevPlayer.id !== removeId)
				}
			}));
			PlayerService.removePlayer(removeId, null, (error) => this.setState({ error: error.message }));
		}

		private onCancelRemovePlayer = () => {
			this.setState(update(this.state, { playerToRemove: { $set: null } }));
		}

		private fetch() {
			this.setState({ isLoading: true, disableAdd: true });
			PlayerService.getPlayers(
				(players) => this.setState({ players, isLoading: false, disableAdd: false }),
				(error) => this.setState({ isLoading: false, error: error.message }),
			);
		}
	}
));
