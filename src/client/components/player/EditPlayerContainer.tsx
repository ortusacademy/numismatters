import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PlayerService } from '../../services/PlayerService';
import { IPlayerModel } from '../../../shared/IPlayer';

import { Loading } from '../app/Loading';
import { EditPlayer } from './EditPlayer';

interface IProps extends RouteComponentProps<{ playerId: string }> {
	player?: IPlayerModel;
}

interface IState {
	error?: string;
	isLoading: boolean;
	player?: IPlayerModel;
}

export const EditPlayerContainer = withRouter(
	class extends React.Component<IProps, IState> {
		constructor(props: IProps) {
			super(props);
			this.state = {
				error: null,
				player: props.player,
				isLoading: false,
			};
		}

		componentDidMount() {
			if (!this.props.player && ((this.props.match.params && this.props.match.params.playerId) || this.props.match.params.playerId !== this.props.player.id)) {
				this.fetchPlayer(this.props.match.params.playerId);
			}
		}

		public render() {
			if (this.state.error) return this.state.error;
			if (this.state.isLoading) return <Loading />;
			return <EditPlayer
				player={this.state.player}
				onSubmit={this.onSavePlayer}
				submitText='Save' />;
		}

		private fetchPlayer = (id: number) => {
			PlayerService.getPlayer(id,
				(player) => this.setState({ player, isLoading: false }),
				(error) => this.setState({ error: error.message }),
			);
		}

		private onSavePlayer = (player: IPlayerModel) => {
			PlayerService.updatePlayer(player as IPlayerModel,
				() => this.props.history.goBack(),
				(error) => console.error(error));
		}
	}
);
