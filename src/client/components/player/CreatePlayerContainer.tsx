import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { PlayerService } from '../../services/PlayerService';
import { IPlayer } from '../../../shared/IPlayer';
import { EditPlayer } from './EditPlayer';

interface IState {
	player: IPlayer;
}

export const CreatePlayerContainer = withRouter(
	class extends React.Component<RouteComponentProps<null>, IState> {
		state: IState = {
			player: {
				firstName: '',
				lastName: '',
				age: 1,
				grade: 1,
			}
		};

		public render() {
			return <EditPlayer
				player={this.state.player}
				onSubmit={(player) => {
					PlayerService.addPlayer(player as IPlayer,
						() => this.props.history.goBack(),
						(error) => console.error(error));
				}}
				submitText='Create' />;
		}
	}
);
