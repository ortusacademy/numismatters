import * as React from 'react';
import { createStyles, WithStyles, Button } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { IPlayerModel } from '../../../shared/IPlayer';
import { RouteComponentProps, withRouter } from 'react-router';

const styles = (theme) => createStyles({
	centered: {
		alignItems: 'center',
		display: 'flex',
		flex: 1,
		justifyContent: 'center',
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4,
	},
});

interface IProps extends RouteComponentProps<any>, Partial<WithStyles<typeof styles>> {
	player: IPlayerModel;
	onRemove: (player: IPlayerModel) => void;
}

interface IState {
	isOpen: boolean;
}

export const PlayerListItem = withRouter<IProps>(withStyles(styles)(
	class extends React.Component<IProps, IState> {
		state: IState = {
			isOpen: false
		};

		public render() {
			const { classes, player } = this.props;
			return (
				<>
					<ListItem button onClick={this.onClick}>
						<ListItemText inset primary={`${player.firstName} ${player.lastName}`} />
						{this.state.isOpen ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse in={this.state.isOpen} timeout='auto' unmountOnExit>
						<List disablePadding>
							<ListItem>
								<Button variant='text' className={classes.centered} onClick={this.onEditPlayer}>
									Edit
								</Button>
							</ListItem>
							<ListItem>
								<Button variant='text' color='secondary' className={classes.centered} onClick={this.onRemovePlayer}>
									Remove
								</Button>
							</ListItem>
						</List>
					</Collapse>
				</>
			);
		}

		private onClick = () => {
			this.setState((state) => ({ isOpen: !state.isOpen }));
		}

		private onEditPlayer = () => {
			this.props.history.push(`/player/${this.props.player.id}/edit`);
		}

		private onRemovePlayer = () => {
			this.props.onRemove(this.props.player);
		}

	}
));
