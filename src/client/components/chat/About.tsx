import * as React from 'react';
import { ResponseBase } from './ResponseBase';

export class About extends ResponseBase {
	componentWillMount() {
		this.respond();
	}

	render() {
		return <div>I was created by <a target='_blank' href='https://keybase.io/sambatista'>Samuel Batista</a>.</div>;
	}
}
