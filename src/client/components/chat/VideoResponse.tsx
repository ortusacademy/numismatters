import * as React from 'react';
import ReactPlayer from 'react-player';
import { ResponseBase } from './ResponseBase';
import { width } from './ChatBotContainer';

export class VideoResponse extends ResponseBase {
	render() {
		return (
			<div>
				<ReactPlayer url={this.props.src} width={width - (width / 10)} onReady={() => this.respond()} />
			</div>
		);
	}
}
