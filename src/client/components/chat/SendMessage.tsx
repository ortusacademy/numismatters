import * as React from 'react';
import { ResponseBase } from './ResponseBase';

export class SendMessage extends ResponseBase {
	componentWillMount() {
		const { steps } = this.props;
		const { message } = steps;
		this.setState({ sending: true, message });
	}

	componentDidMount() {
		this.setState({ sending: false }, this.props.triggerNextStep);
	}

	render() {
		const { sending } = this.state;
		return <em>{sending ? 'Sending...' : 'Done'}</em>;
	}
}

