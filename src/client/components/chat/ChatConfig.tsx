import * as React from 'react';
import { Balance } from './Balance';
import { PrivacyAndSecurity } from './PrivacyAndSecurity';
import { VideoResponse } from './VideoResponse';
import { About } from './About';
import { SendMessage } from './SendMessage';

let menu_count = 0;
// const help_menu_count = 0;
export const ChatConfig = [
	{
		id: 'welcome',
		// hideInput: true,
		message: 'Welcome to Ortus Academy!',
		trigger: 'menu'
	},
	{
		id: 'menu',
		hideInput: true,
		options: [
			{ value: menu_count++, label: 'Balance', trigger: 'balance' },
			{ value: menu_count++, label: 'Send Message', trigger: 'send' },
			// { value: menu_count++, label: 'Send Eth & Tokens', trigger: 'send_eth_and_tokens' },
			// { value: menu_count++, label: 'Email Eth & Tokens', trigger: 'email_eth_and_tokens' }, // todo
			// { value: menu_count++, label: 'Help & Tips', trigger: 'help' },
			{ value: menu_count++, label: 'About', trigger: 'about' },
		]
	},
	{
		id: 'balance',
		asMessage: true,
		// hideInput: true,
		waitAction: true,
		component: <Balance />,
		trigger: 'menu',
	},
	{
		id: 'send',
		message: 'What is the message?',
		trigger: 'message',
	},
	{
		id: 'message',
		user: true,
		trigger: 'send-end',
	},
	{
		id: 'send-end',
		asMessage: true,
		waitAction: true,
		component: <SendMessage />,
		trigger: 'menu'
	},
	/* {
		id: 'help',
		// hideInput: true,
		message: 'How can I help?',
		trigger: 'help_menu',
	},
	{
		id: 'help_menu',
		// hideInput: true,
		waitAction: true,
		options: [
			{ value: help_menu_count++, label: 'What is cryptocurrency?', trigger: 'what_is_cryptocurrency' },
			{ value: help_menu_count++, label: 'What is blockchain?', trigger: 'what_is_blockchain' },
			{ value: help_menu_count++, label: 'What is ethereum?', trigger: 'what_is_ethereum' },
			{ value: help_menu_count++, label: 'Privacy & Security', trigger: 'privacy_and_security' },
			{ value: help_menu_count++, label: 'Something Else', trigger: 'something_else' }
		]
	}, */
	{
		id: 'what_is_cryptocurrency',
		// hideInput: true,
		waitAction: true,
		component: <VideoResponse src='https://www.youtube.com/watch?v=5MfbuQnGM2M' />,
		trigger: 'menu',
	},
	{
		id: 'what_is_blockchain',
		// hideInput: true,
		waitAction: true,
		component: <VideoResponse src='https://www.youtube.com/watch?v=6WG7D47tGb0' />,
		trigger: 'menu',
	},
	{
		id: 'what_is_ethereum',
		// hideInput: true,
		waitAction: true,
		component: <VideoResponse src='https://www.youtube.com/watch?v=TDGq4aeevgY' />,
		trigger: 'menu',
	},
	{
		id: 'privacy_and_security',
		// hideInput: true,
		waitAction: true,
		component: <PrivacyAndSecurity />,
		trigger: 'menu',
	},
	{
		id: 'something_else',
		// hideInput: true,
		waitAction: true,
		component: <PrivacyAndSecurity />,
		trigger: 'menu',
	},
	{
		id: 'about',
		asMessage: true,
		// hideInput: true,
		waitAction: true,
		component: <About />,
		trigger: 'menu',
	}
];
