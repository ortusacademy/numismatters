import * as React from 'react';
import { Loading } from 'react-simple-chatbot';

export class ResponseBase extends React.Component<any, any> {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			response: ''
		};
	}

	respond(response?: string) {
		this.setState({ loading: false, response: response });
		this.props.triggerNextStep();
	}

	render() {
		const { loading, response } = this.state;
		return (
			<div>
				{loading ? <Loading /> : response}
			</div>
		);
	}
}
