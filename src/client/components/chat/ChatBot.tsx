import * as React from 'react';
import ReactSimpleChatBot from 'react-simple-chatbot';
import { ThemeProvider } from 'styled-components';
import { ChatBotContainer } from './ChatBotContainer';
import { width, height } from './ChatBotContainer';
import { ChatConfig } from './ChatConfig';

export const ChatBot = () => (
	<ChatBotContainer>
		<ThemeProvider theme={{
			background: '#ffffff',
			fontFamily: 'Helvetica Neue',
			headerBgColor: '#f5f8fb',
			headerFontColor: '#4a4a4a',
			headerFontSize: '16px',
			botBubbleColor: '#f5f8fb',
			botFontColor: '#4a4a4a',
			userBubbleColor: '#fff',
			userFontColor: '#4a4a4a'
		}}>
			<ReactSimpleChatBot
				headerTitle='OrtusBot'
				placeholder='Response ...'
				botAvatar='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAIAAADYYG7QAAABG2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS41LjAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+Gkqr6gAAAYNpQ0NQc1JHQiBJRUM2MTk2Ni0yLjEAACiRdZG7SwNBEIe/PETRSAQtLESCRAsx4gNEG4sEjYJaJBF8NcmZh5DE4y5Bgq1gG1AQbXwV+hdoK1gLgqIIYmNjrWij4ZxLAgliZpmdb3+7M+zOgjWUVFK6fQBS6YwW8HtdC4tLrvpX7HRiY4jesKKrs8HJEDXt6wGLGe88Zq3a5/61ptWoroClQXhcUbWM8JTwzEZGNXlXuE1JhFeFz4X7NLmg8L2pR0r8ZnK8xD8ma6GAD6wtwq54FUeqWEloKWF5Oe5UMquU72O+xBFNzwcldol3oBPAjxcX00zgY4RBxmQewSP96ZcVNfIHivlzrEuuIrNKDo014iTI0CdqVqpHJcZEj8pIkjP7/7evemx4qFTd4YW6F8P46Ib6HSjkDeP72DAKJ2B7hqt0JX/9CEY/Rc9XNPchOLfg4rqiRfbgchvan9SwFi5KNnFrLAbvZ9C8CK230Lhc6ll5n9NHCG3KV93A/gH0yHnnyi+pH2gEQTWnMQAAAAlwSFlzAAAOxAAADsQBlSsOGwAADM5JREFUWIWtWX1YU9cZfy8kIV8kAULChySABETly1EUVxSnUi0U8Yt2orMWZZapbWVSn9mpccVnc9qOsVpnC0pd91AtpiqzgN8gIioMA+VTIEBIQsJHEghJSHLv/riUQkgg9tn7T3LP+b2/93fee+6557wXwTAMXtIaGxvFYnF7e3tDQ0NXV1dDQ4PJZAIABEFCQkL8/f0jIyODgoJCQ0OXL1/+suSAOWxms/nzzz/n8/lWDHw+PzY2dtmyWC8vb6suOp1+/PjxoaEhx6M4JKizszMzM9PLywtPQ1pa2rlz5+7cuSOV9uEAFEVLy8rw/10SSXl5+dmzZ9PS0hAEAQAmk7l9+/bnz5//HwTp9fq8vDw3NzcAWLRokVAoVKlUM2EvXnQcPy6c2T4wMHDy5MklS5YAAJlMPnbs2Ojo6M8X1N3dHRISAgAUCiU3N9diQe0h791/sC0tbRaqb7/9ls1mA4C7u3tzc/PPEXTz5k0ejwcAW7dubW1tnYUCw7Diq98tDgubHdPX13fw4EEA8PLyKiwsfDlBRUVFDAaDwWAIhUKTyTR7JAzDzp37gsfnV1RUzIks+uYbX19fCoVy8eJFRwWJRCL8GSkuLp4zAIZhFotFeCKHx+dnZ2c7gheLxSwWCwAuXbo0t6Dbt2/jT5NIJHKEHcMwsbgBFxQQEOCgS11dHZPJpFAoM8c8TdDo6GhkZCSJRCoqKnKQGsOw767dwAUBgFKpdNCrvr6ezWbzeLze3t6p7U5T17Ft27bV19fv3bv3zTffdHBdlcvlTU3Nk5fl5eUOOkZERBw9erSnpycpKQmb+raYlFZSUgIA8fHxOp1uqmSTyVRT88RqfFVVVTt37lwUEh4RsFx4ImcyQ960+QK/xZs3b87Pz7dyGR4erq+3Xht37NgBAAUFBdYZMhgMOTk5ACAUCqlU6tShEAgEiUTy0UcfdXZ2AoBUKt2zZ8+mhLSOO2MxxO3xoZunghku7FWsPZg44PSh/LjY+NLSUgAwmUx1dfV//OOxgAB/qzwdOXKEQqEIhcKhoSG8ZUJQfn5+dXV1amrqihUrZqY3JWUD0YXyy1dfPXjwoEAgaC8f3RD4wUK3V4mIC4GOTkVKdc0AwCb7xfm+yVPHb9+UfuLEiYuFl9J3p69du5rBYFgxh4SEHD58uLu7+/Tp09MEFRcXM5nMvLw8W7cbSCTSrp2/2bIl9V//+pqE0T1c5v3U5TFNkM6oHR6XT2SL5rZry37jOPrF+X/yeX7r16+3SZ6dne3n5ze51jgBgFQqffDgQUJCAofDsekDAACYady0ZWsqkE3Xu/5eq/zehBkRApC9zFa4bm0jAJB9zeyVelcBeqv8+9pntYGBgQQCwSYvmUxOTExsaWm5e/fuhKBTp06hKJqYmGRPi8lkWr16jawC5Xr4/O53B6g0Sm1/6dfNx4bpbYBYg0ewfvZKvcdyA5GFikTFT58+ifZa/9W5y/hDY9NSU1MB4NNPPwUABEVRDoeDomh3dzedTrfpUFhY+Jff/zPO+y3afBNriVGhkH975bJKpVyzJmH165vo3ECCi2uvtFerVcskbW1197du2ggAJTeuPX36xI+xIIG3x2gZbaOWVT66RyKRbIYQCAQdHR1KpdJJLBYPDAwkJCTYU2M2mz/P+2IpZwMAjEmIJrWTl5f3lq2pbA43YtUWTtQbcpeAujGGgrlw3P+Xr2zMzPzTRRonAFcDAEs465wRApXAGumCgoIL9pL01ltvYRjW2NhI6OrqAoCYmBh70KtXrw53jpPmUQAAs8DgQwp3vc7L2+dkwS2zi9vfHnX1qA1T8WsCPZIXxqPkSwAQwY3nUgLw9gXsZdevX9u797c2o/j7+wNAU1MTQSqVAoCPj489QZWVld70+QCgNal6R5qHDPKR87LwFQkpy93zqrpkI0Yr/J3OQSrJ+f1j/+iVtCNKsmnQONQgG5NrPV34ZbU37EXh8/0BQKFQEFQqFQDgr18rU6vVdXV11dXVAxrD84G7ar1ysmv/G9u/b1PNVAMAGMD1FmWUT5CvIBwEAAD85LBxjX64uV//w29qxB0RoQFkopOVl5/fPACQyWQEo9EIAN7eP+3PW1paLl++IhJdra+vtz0awSIvv6D7/2myN1wA6BrSe1B/mr8kJoW7zJ+7LL1VDa3V/a5kZ38Oxc+DzHYl4gAiiYSHnlgbpH199+7fb29rr62tnVzFg4NDbAabFxSqM5rRWY9Pegs6ZrIobKUQt+eyEQAgERA3GtGVSnAnTiiZ+Jnn6/urVau6uiR9fbLBwaF+pVKtVtvjcmFwXIjOs8kBcAJoVo4W/rdvFoyATQ3zcQ0kUAK4lMXMiQWNgJ9UZDJ5eHh4aOiC0NAFeIdGo+nslDQ1N0sk3RaLZSrR+IiK4IT4Mcm9GgPYMU8aqVejn9nujCCbI7nxQW4p4Ryqy09rd1tbGwDQ6XQCl8sFgJERrZUnk8mMioqIiorYuXOnroXux51PoKFkHwuRZcEwbETWkhrG/9sjicXWneOzKIHuVNEPCvzSe2CUrR6j68bFbV811F5xd3ef6YLPk7CwMIKHhwcA9PXZza1AICgufeChDwEA7Q/gRMYa4Do7oDVp97E18z1uvxi0TD+M+zJcMpfyuns7Q0xtwb2YW6ueaEYBwGgZ82XrbKoBgJ6eHgBgs9mEoKAgAHjxosOeoF27dv3jz/mTl7W95U8Vd+EpsFhur/86ayGH/o1Yjj//Tgj8KtBjfTBH2v58rKMykk5HBYiilYoBAgDtmiexa5baiyKRSACAx+MhWq2WwWD4+PjMkqR9+/Z13rT40UMHDVLRi09QzJKUlPxKzNLGNgnDd8GK1389bkaNZtSVTDCNqaVNj08c2s3hcLdsTfX05Gh/II00kQDgdv/57yuvBgcH2wyRkpJy7dq12tpaJ1dX140bN8pkslm2wxkZGU8GRXqLtk5ZhmKWxKTkV2KWAsCobCD/L1m71wacei+l436h7Nl3fU9F0ue1TkDAX8AajcY1ZJzkhjarH27L2GhPjVwuv1FSwuVyo6KinADg3XffBYDJLdJMCw8PzzklFL0406URp6RsiolZipmRocdkljwMAEzjxvbGZxpl77huGADcUH5aqJBD4ykU8rOf/V2r06CC3mb9vcOHD9vjP3/+PGqxZGRkIAjiBABr164NDg6+efOmPQcASEvb7kzBEpOSo5b8QtdJVJZT9L0EV4IHi2y9pxsfdHZxor4RcGCVXxpqQv799SWEYr5Y+BWNRrNHfuvWLVdX1wMHDsDkFjY5Obmnp8feFhYA9u/f92rcytjY2DLRPVmNwaybcOQxQqfhMDAOOAOAM0IMYkUv902hUin/KblRVVWt0WhsMj98+LCqqmrdunV4NWKCNzs7m0qlfvDBBwMDAzN9ysrK1GrtoUO/P/KHw5lZb5dIcyUjYryLSfSaijQOOqMGBAD0Fm1Z95dGX0lNTc2VK5e/KrxQVn5rJrPBYMC3i/v378dbJgR5enoeOXLEYrHg+0grH7Vac+7c2ZUr4gAgLS2t6nHF4k3M77rOPFaIRs1DU8ED0uHnA7dv916QcyvO5B9/9OgRm82Ojo5ua2s16PUNDY1W5AUFBXK5PD09PS4u7scc/2j9/f18Ph9BnO7cuTP1LDc+Pm7zLKxSqc588smOHTvwl8876RnCEzm/3ftuVlZWaWmpTRerQkpdXR2DwaDRaFPLPdPO9g0NDSQSicfjGY1Gm4w2bdmy2ElBcrnCccfo6GgAKCkpmdo4baO0ePHiDz/8sKenJy4uTiaTzbzlNm3Dhg34H2dnZ3xizmk6nS4hIeHZs2f79u1LTEyc1jdTeHp6OgCsW7dOKpU6MtDm5mY8Q5evOFRP0mq1b7/9NgC89tprY2NjVr02BBkMhpUrVwJAdHS0VeHBnnG53HfSM+r+W+8IGJ+/kZGRNsltl/SMRuPu3bsBAD/kzhlj9erV76RnzFlhraysDF24EACSk5PtDXW2KmxOTg6FQgGAzMzM9vb2WZDvvffeoezDswAGBwePHj1KIpEQBDl06JDFYrGHnKNO3dHRIRAI8Nn2/vvvq9Vqm7Avvvzyr6fP2CPJzc0lEokA4O3tXVNTM3vEuSv5arX6s88+w+sQTBYrNTU1Ly+vu7t7KkYmk124MK2qqtPpioqKdu3aNW/ePAAgk8kff/yxTCabMxyCOfbxxWQy5ebm3rhxo6KiAgAQBImOjo6JieHz+TQajUQiSfv6uByO2WxWKBQ1T548rq7W6XQAsGjRoqSkpKysLE9PT0cCvcTHF9wUCsXJkycXLFgwO62Pj8+ePXvEYvHL8juaoZk2NDTU3t6uVCpVKpXBYMDT5u3tzWKxeDxeYGDgz6P9HyCyxa03OqydAAAAAElFTkSuQmCC'
				enableMobileAutoFocus={true}
				hideUserAvatar={true}
				hideBotAvatar={false}
				botDelay={0} /*ms*/
				userDelay={0} /*ms*/
				steps={ChatConfig}
				width={`${width}px`}
				height={`${height}px`}
			/>
		</ThemeProvider>
	</ChatBotContainer>
);
