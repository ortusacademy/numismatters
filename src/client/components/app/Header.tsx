import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { RouteComponentProps, withRouter } from 'react-router-dom';

export const Header = withRouter(
	class extends React.Component<RouteComponentProps<any>> {
		public render() {
			return (
				<header>
					<AppBar position='static'>
						<Toolbar>
							<Typography variant='title' color='inherit' className={'nm-flex'}>
								Numis Matters
							</Typography>
							<Button color='inherit' onClick={this.onGoHome}>Home</Button>
						</Toolbar>
					</AppBar>
					<hr />
				</header>
			);
		}

		private onGoHome = () => {
			this.props.history.push('/');
		}
	}
);
