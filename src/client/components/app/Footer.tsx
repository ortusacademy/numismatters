import * as React from 'react';

export const Footer = () => (
	<footer className='footer'>
		<hr />
		<p>Numis Matters © <a href='http://www.ortusacademy.com/' target='__blank'>Ortus Academy</a></p>
	</footer>
);
