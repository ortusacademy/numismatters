import { createStyles, WithStyles } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import * as React from 'react';

const styles = (theme) => createStyles({
	loading: {
		textAlign: 'center'
	}
});

export const Loading = withStyles(styles)(
	class extends React.Component<WithStyles<typeof styles>> {
		public render() {
			const { classes } = this.props;
			return (
				<div className={classes.loading}>
					<CircularProgress size={50} />
				</div>
			);
		}
	}
);
