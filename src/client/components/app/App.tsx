import * as React from 'react';
import ErrorBoundary from 'react-error-boundary';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { GameList } from '../game/GameList';
import { PlayerList } from '../player/PlayerList';
import { EditOrCreateGameContainer } from '../game/EditOrCreateGameContainer';
import { CreatePlayerContainer } from '../player/CreatePlayerContainer';
import { EditPlayerContainer } from '../player/EditPlayerContainer';
import { SelectStation } from '../game/SelectStation';
import { Bank } from '../game/station/Bank';
import { Bills } from '../game/station/Bills';
import { Fun } from '../game/station/Fun';
import { Investments } from '../game/station/Investments';
import { Lottery } from '../game/station/Lottery';
import { RealEstate } from '../game/station/RealEstate';
import { SelectPlayer } from '../player/SelectPlayer';
import { NotFound } from './NotFound';
import { Header } from './Header';
import { Footer } from './Footer';
import { Chat } from './Chat';

export type MatchGame = { gameId: string };
export type MatchPlayer = { playerId: string };
export type MatchRoom = MatchGame & MatchPlayer;

export const App = () => (
	<BrowserRouter>
		<ErrorBoundary>
			<Header />
				<main className='content'>
						<Switch>
							<Route exact path='/' component={GameList} />
							<Route exact path='/players' component={PlayerList} />
							<Route exact path='/player/create' component={CreatePlayerContainer} />
							<Route exact path='/player/:playerId/edit' component={EditPlayerContainer} />
							<Route exact path='/games' component={GameList} />
							<Route exact path='/game/create' component={EditOrCreateGameContainer} />
							<Route exact path='/game/:gameId/edit' component={EditOrCreateGameContainer} />
							<Route exact path='/game/:gameId/start' component={SelectPlayer} />
							<Route exact path='/home/:gameId/:playerId' component={SelectStation} />
							<Route exact path='/lobby/:gameId/:playerId' component={Chat} />
							<Route exact path='/bank/:gameId/:playerId' component={Bank} />
							<Route exact path='/bills/:gameId/:playerId' component={Bills} />
							<Route exact path='/fun/:gameId/:playerId' component={Fun} />
							<Route exact path='/investments/:gameId/:playerId' component={Investments} />
							<Route exact path='/lottery/:gameId/:playerId' component={Lottery} />
							<Route exact path='/real-estate/:gameId/:playerId' component={RealEstate} />
							<Route component={NotFound} />
						</Switch>
				</main>
			<Footer />
		</ErrorBoundary>
	</BrowserRouter>
);
