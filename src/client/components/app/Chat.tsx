import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Room } from 'colyseus.js';
import { MatchRoom } from './App';
import { Loading } from './Loading';
import { ChatBot } from '../chat/ChatBot';
import { ChatService } from '../../services/ChatService';

interface IProps extends RouteComponentProps<MatchRoom> {}

interface IState {
	lifetime: number;
	messages: string[];
	isLoading: boolean;
}

export const Chat = withRouter(
	class Chat extends React.Component<IProps, IState> {
		state: IState = {
			lifetime: 0,
			messages: [],
			isLoading: true
		};

		static instance: Chat;
		private chatRoom: Room;

		constructor(props: IProps) {
			super(props);
			this.chatRoom = ChatService.joinRoom(props.match.params.gameId);
			this.chatRoom.onJoin.add(this.onJoin);
			this.chatRoom.onMessage.add(this.onMessage);
			this.chatRoom.onStateChange.add(this.onStateChange);
			Chat.instance = this;
		}

		onJoin = () => {
			this.chatRoom.send({ type: 'joined', id: this.props.match.params.playerId });
			this.setState({ isLoading: false });
		}

		onMessage = (lifetime) => {
			this.setState({ lifetime });
		}

		onStateChange = (newState, patches) => {
			this.setState({ messages: newState.messages });
		}

		render() {
			if (this.state.isLoading) return <Loading />;
			return <div className='chatBot'><ChatBot/></div>;
		}

		componentWillUnmount() {
			this.chatRoom.leave();
		}

		static sendMessage(message) {
			this.instance.chatRoom.send(message);
		}
	}
);
