import axios from 'axios';
import { IPlayer, IPlayerModel } from '../../shared/IPlayer';

export class PlayerService {
	public static getPlayers = (cb: (players: IPlayerModel[]) => void, err: (error: Error) => void) => {
		axios.get('/api/player/all').then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static getPlayer = (id: number, cb: (player: IPlayerModel) => void, err: (Error) => void) => {
		axios.get(`/api/player/${id}`).then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static addPlayer = (player: IPlayer, cb: (player: IPlayerModel) => void, err: (error: Error) => void) => {
		axios.post('/api/player', player).then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static updatePlayer = (player: IPlayerModel, cb: (player: IPlayerModel) => void, err: (Error) => void) => {
		axios.put(`/api/player/${player.id}`, player).then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static removePlayer = (id: number, cb: () => void, err: (Error) => void) => {
		axios.delete(`/api/player/${id}/remove`).then(() => cb && cb()).catch((reason) => err(reason));
	}
}
