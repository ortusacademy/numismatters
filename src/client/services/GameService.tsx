import axios from 'axios';
import { IGame, IGameModel } from '../../shared/IGame';
import { IPlayerModel } from '../../shared/IPlayer';

export class GameService {
	public static getDefaultGame = (cb: (games: IGame) => void, err: (Error) => void) => {
		axios.get('/api/game/default').then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static getGame = (id: number, cb: (game: IGameModel) => void, err: (Error) => void) => {
		axios.get(`/api/game/${id}`).then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static getGames = (cb: (games: IGameModel[]) => void, err: (Error) => void) => {
		axios.get('/api/game/all').then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static getPlayersInGame = (id: number, cb: (players: IPlayerModel[]) => void, err: (Error) => void) => {
		axios.get(`/api/game/${id}/getPlayers`).then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static addGame = (game: IGame, cb: (game: IGameModel) => void, err: (Error) => void) => {
		axios.post('/api/game', game).then((response) => cb && cb(response.data)).catch((reason) => err(reason));
	}

	public static updateGame = (game: IGameModel, cb: () => void, err: (Error) => void) => {
		axios.put(`/api/game/${game.id}/update`, game).then(() => cb && cb()).catch((reason) => err(reason));
	}

	public static removeGame = (id: number, cb: () => void, err: (Error) => void) => {
		axios.delete(`/api/game/${id}/remove`).then(() => cb && cb()).catch((reason) => err(reason));
	}
}
