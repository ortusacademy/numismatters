import { Client, Room } from 'colyseus.js';
import { COLYSEUS_PORT } from '../../shared/config';

const host = window.document.location.host.replace(/:.*/, '');
const protocol = location.protocol.replace('http', 'ws');
const isLocalHost = host === 'localhost';

export class ChatService {
	public static readonly colyseus = new Client(`${protocol}//${host}:${COLYSEUS_PORT}`);
	public static joinRoom(id: string): Room {
		const room = this.colyseus.join(id);
		if (room && isLocalHost) {
			room.onJoin.add(() => console.log(this.colyseus.id, 'onJoin', room.name));
			room.onMessage.add((payload, patches) => console.log(this.colyseus.id, 'onMessage:', payload, 'patches:', patches));
			room.onStateChange.add((newState, patches) => console.log(this.colyseus.id, 'onStateChange:', newState, 'patches:', patches));
			room.onError.add(() => console.error(this.colyseus.id, "couldn't join", room.name));
		}
		return room;
	}
}
