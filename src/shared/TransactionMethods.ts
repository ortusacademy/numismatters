export enum TransactionMethods {
	Cash = 'Cash',
	Debit = 'Debit',
	Credit = 'Credit',
}
