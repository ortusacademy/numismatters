export enum TransactionTypes {
	Withdrawal = 'Withdrawal',
	Deposit = 'Deposit',
	SalaryClaim = 'SalaryClaim'
}
