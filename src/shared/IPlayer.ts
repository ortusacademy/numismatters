import { IBaseModel } from './IBaseModel';
import { IGameModel } from './IGame';
import { ITransaction } from './ITransaction';

// keep IPlayer in sync with src/server/models/player.ts
export interface IPlayer {
	age: number;
	firstName: string;
	games?: IGameModel[] | any[]; /* We don't want to include Game model on the client, so we add any so classes that implement IPlayerEntity compile */
	grade: number;
	lastName: string;
	transactions?: ITransaction[]; /* We don't want to include Transaction model on the client, so we add any so classes that implement IPlayerEntity compile */
}

export interface IPlayerModel extends IPlayer, IBaseModel { }
