export interface IBaseModel {
	id: number;
	uuid: string;
	createdAt: Date;
	updatedAt: Date;
}
