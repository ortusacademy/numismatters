import { IBaseModel } from './IBaseModel';
import { IPlayer } from './IPlayer';
import { ITransaction } from './ITransaction';

// keep IGame in sync with src/server/models/game.ts
export interface IGame {
	name: string;
	creditFee: number;
	debitFee: number;
	difficulty: number;
	lateFee: number;
	playerRounds?: number[];
	players?: IPlayer[] | any[]; /* We don't want to include Player model on the client, so we add any so classes that implement IGameModel compile */
	rounds: number;
	startingSalary: number;
	transactions?: ITransaction[] | any[]; /* We don't want to include Transaction model on the client, so we add any so classes that implement IGameModel compile */
}

export interface IGameModel extends IGame, IBaseModel { }
