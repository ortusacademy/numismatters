// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

export enum StatusCode {
	// 2xx Success
	OK = 200,
	// 4xx Client errors
	BadRequest = 400,
	Unauthorized = 401,
	Forbidden = 403,
	NotFound = 404,
	// 5xx Server errors
	Error = 500,
	NotImplemented = 501
}
