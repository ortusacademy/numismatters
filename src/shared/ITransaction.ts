import { IBaseModel } from './IBaseModel';
import { IGameModel } from './IGame';
import { IPlayerModel } from './IPlayer';
import { TransactionMethods } from './TransactionMethods';
import { TransactionTypes } from './TransactionTypes';

// keep this in sync with src/server/models/transaction.ts
export interface ITransaction {
	amount: number;
	game?: IGameModel | any; /* We don't want to include Game model on the client, so we add any so classes that implement ITransaction compile */
	method: TransactionMethods;
	player?: IPlayerModel | any; /* We don't want to include Player model on the client, so we add any so classes that implement ITransaction compile */
	round: number;
	type: TransactionTypes;
}

export interface ITransactionModel extends ITransaction, IBaseModel { }
